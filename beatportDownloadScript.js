// Version 1.1

// ----------------------------------------------------------------------------------------------------------------
// CONFIGURATION
// ----------------------------------------------------------------------------------------------------------------

var timeoutBetweenDownloadsInSeconds = 1.5;





// ----------------------------------------------------------------------------------------------------------------
// DON'T TOUCH ANYTHING BELOW HERE IF YOU DON'T KNOW WHAT YOU'RE DOING!
// ----------------------------------------------------------------------------------------------------------------

var currentDownload = 0;
var totalTimeout = 0;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function startDownload (downloadLink) {
  console.log("Downloading from "+downloadLink);

  var link = document.createElement("a");
  link.href = downloadLink;
  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);
  delete link;
}

function getDownloadLinkAndStartDownload(url, callback) {
	var request = new XMLHttpRequest();
	request.open('GET', url, true);

	request.onload = function() {
	  if (request.status >= 200 && request.status < 400) {
	    // Success!
	    callback(JSON.parse(request.responseText).download_url);
	  } else {
	    // We reached our target server, but it returned an error
	  }
	};

	request.onerror = function() {
	  // There was a connection error of some sort
	};

	request.send();
}

function getApiLinkAndStartDownload(downloadButtonParentDivElement, currentDownload) {
	var downloadId = downloadButtonParentDivElement.dataset.downloadId
	var requestUrl = "https://www.beatport.com/api/downloads/purchase?downloadId="+downloadId;
	
	totalTimeout = currentDownload*timeoutBetweenDownloadsInSeconds*1000;

	sleep(totalTimeout).then(()=>{
	    getDownloadLinkAndStartDownload(requestUrl, startDownload);
    });
	
}

var downloadButtonParentElements = document.getElementsByClassName("download-button-parent");
var downloadButtonParentDivElements = Array.prototype.filter.call(downloadButtonParentElements, function(element) {
	return element.nodeName === "DIV";
}).forEach(function(downloadButtonParentDivElement){
	currentDownload = currentDownload+1;
	getApiLinkAndStartDownload(downloadButtonParentDivElement, currentDownload);
});