# README #

INFO: Beatport seems to have realized that this is a must have feature and now have a "Download All Beta". Didn't work properly for me as of 08.07.2020, but maybe it works for you.

### What is this repository for? ###

I wrote this script because Beatport doesn't have a "Download all purchased tracks" function. AFAIK Beatport Desktop would have that function, but it is only available for Mac users.

The Windows version isn't maintained anymore by Beatport, so Windows users have to manually download each track by clicking on one download button after the other, which can be quite tedious if a lot of tracks are purchased at once.

Feel free to use this script in any way you would like to.

Version 0.9 - Should work but is not tested yet.
Version 1.0 - Timeout fixed - Tested with 20 Tracks - Worked like a charm.
Version 1.0 - Changed default timeout to 1.5 seconds

### How do I get set up? ###

The configuration can be made in the variable(s) that are set in the first few lines of the script.

| Variable                         | Description                                                                                                         |
| -------------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| timeoutBetweenDownloadsInSeconds | You can configure how long the script should wait between two downloads to prevent it from downloading all at once. |
|                                  | Set to 0 if you want to start all the downloads at once. Not recommended if you purchased a lot of tracks at once.  |
|                                  | Default = 1                                                                                                         |

### How do I run the script? ###

1. Buy some tracks on Beatport
2. On the page where you normally would download the files one by one press F12 to open the developer tools
3. Open the console tab
4. Copy and paste the code in beatportDownloadScript.js into the the console.
5. Run the script by pressing the enter key. The script should now run and by default start one download each second until all downloads are started.

Hit me up if you need any help or run into problems.

### Who do I talk to? ###

Sascha Kaufmann aka Dr. Gadget

Mail: kaufmsas@gmail.com

Soundcloud: https://soundcloud.com/drgadget

Instagram: https://www.instagram.com/drgadget604